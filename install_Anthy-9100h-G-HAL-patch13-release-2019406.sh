#!/bin/sh
#
# Linux is a Registered Trademark of Linus Torvalds.
#
# Slackware is a registered trademark 
# of Patrick Volkerding and Slackware Linux, Inc.
#
# Acknowledgments
# To write this script, I referred to https://slackware.jp/packages/anthy.html
#
#==============================================================================
#
# Copyright 2019 EarthSimilarityIndex, the 3rd Planet, Solar System
#
# Released under the MIT license
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included
# in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
# OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
# IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
# DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
# TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
# OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
#

SLACK_DESC=$(cat <<EOS
anthy: anthy 9100h a Japanese Input Method Engine  
anthy:  
anthy: Anthy is a Japanese Input Method Engine. This package contains Anthy 
anthy: 9100h with G-HAL's Patch.  
anthy: 
anthy: 
anthy: 
anthy: 
anthy: http://anthy.osdn.jp/  
anthy: 
anthy: http://www.fenix.ne.jp/~G-HAL/soft/nosettle/im.html#anthy  
EOS
)

if [ ! $USER = root ]; then
	echo "run this script as root"
	exit
else
	cd || exit 1
	. /etc/profile
	test -d /root/Downloads || mkdir /root/Downloads
	cd /root/Downloads || exit 1
	if ! find /usr/lib64/libtokyocabinet.so; then
		test -d /root/Downloads/tokyocabinet &&
			rm -r "/root/Downloads/tokyocabinet"
		rm -r "tokyocabinet.tar.gz"
		wget https://slackbuilds.org/slackbuilds/14.2/system/tokyocabinet.tar.gz \
			-O tokyocabinet.tar.gz || exit 1
		tar zxf tokyocabinet.tar.gz
		cd /root/Downloads/tokyocabinet
		wget http://ponce.cc/slackware/sources/repo/tokyocabinet-1.4.43.tar.gz \
			-O tokyocabinet-1.4.43.tar.gz || exit 1
		./tokyocabinet.SlackBuild
		installpkg /tmp/tokyocabinet-1.4.43-x86_64-1_SBo.tgz
		cp /tmp/tokyocabinet-1.4.43-x86_64-1_SBo.tgz /root/Downloads/
		ldconfig
		cd /root/Downloads || exit 1
	fi
	test -d /root/Downloads/anthy-9100h &&
		rm -r "/root/Downloads/anthy-9100h"
	rm -r "anthy-9100h.tar.gz"
	wget https://osdn.jp/projects/anthy/downloads/37536/anthy-9100h.tar.gz \
		-O anthy-9100h.tar.gz || exit 1
	tar -zxvf anthy-9100h.tar.gz
	rm -r "anthy-9100h.patch13-release-2019406.alt-depgraph-120315-angie.zipdic-201101.tar.xz"
	wget http://www.fenix.ne.jp/~G-HAL/soft/nosettle/anthy-9100h.patch13-release-2019406.alt-depgraph-120315-angie.zipdic-201101.tar.xz \
		-O anthy-9100h.patch13-release-2019406.alt-depgraph-120315-angie.zipdic-201101.tar.xz || exit 1
	tar -Jxvf anthy-9100h.patch13-release-2019406.alt-depgraph-120315-angie.zipdic-201101.tar.xz
	cd /root/Downloads/anthy-9100h || exit 1
	./configure --prefix=/usr --sysconfdir=/etc --libdir=/usr/lib64
	make
	test -d /tmp/build/anthy-9100h &&
		rm -r "/tmp/build/anthy-9100h"
	test -d /tmp/build || mkdir /tmp/build
	make install DESTDIR=/tmp/build/anthy-9100h
	cd /tmp/build/anthy-9100h || exit 1
	mkdir install
	echo "$SLACK_DESC" >install/slack-desc
	TIMESTAMP=$(date +%Y%m%d%H%M%S%N)
	makepkg -l y -c y /tmp/build/anthy-9100h-x86_64-$TIMESTAMP.txz
	upgradepkg /tmp/build/anthy-9100h-x86_64-$TIMESTAMP.txz ||
	installpkg /tmp/build/anthy-9100h-x86_64-$TIMESTAMP.txz
	cp /tmp/build/anthy-9100h-x86_64-$TIMESTAMP.txz /root/Downloads/
	ldconfig
	test -f /usr/doc/tokyocabinet-1.4.43/README &&
		less /usr/doc/tokyocabinet-1.4.43/README
	test -f /root/Downloads/anthy-9100h/AUTHORS.patch &&
		iconv -f EUCJP -t UTF-8\
			/root/Downloads/anthy-9100h/AUTHORS.patch |
				LANG=ja_JP.UTF-8 less
	dialog --title "INSTALLATION HAS FINISHED"\
		--msgbox "MAKE SURE TO REMOVE '~/.anthy/last-record*' BEFORE USING NEW ANTHY." 5 71
	unset TIMESTAMP
	unset SLACK_DESC
fi
cd || exit 1
exit 0
