#!/bin/sh
#
# Linux is a Registered Trademark of Linus Torvalds.
#
# Debian is a Registered Trademark of Software in the Public Interest, Inc.
#
# Slackware is a registered trademark
# of Patrick Volkerding and Slackware Linux, Inc.
#
#==============================================================================
#
# Copyright (c) 2019 EarthSimilarityIndex, the 3rd Planet, Solar System
#
# Released under the MIT license
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included
# in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
#

SLACK_DESC=$(cat <<EOS
lv: lv 4.51-7 a multilingual pager and grep  
lv:  
lv: lv is a powerful multilingual pager and grep. lv can detect the text 
lv: encoding automatically.  
lv: 
lv: 
lv: 
lv: 
lv: http://www.mt.cs.keio.ac.jp/person/narita/lv/  
lv: 
lv: http://http.debian.net/debian/pool/main/l/lv/  
EOS
)

LV_4_51_7_SHA256SUMS=$(mktemp)

test -L / && exit
test -L /var && exit
test -L /var/log && exit
test -L /var/log/packages && exit
test -L /tmp && exit
test -L /tmp/build && exit
test -L /root && exit
test -L /root/Downloads && exit

if test ! "$USER" = root; then
  echo "Run this script as root, bye."
  exit
fi

test -d /root/Downloads || mkdir /root/Downloads
cd /root/Downloads || exit
rm "lv-4.5.1-7" || rm -r "lv-4.5.1-7"
mkdir $(pwd)/lv-4.5.1-7
cd $(pwd)/lv-4.5.1-7 || exit

wget http://http.debian.net/debian/pool/main/l/lv/lv_4.51-7.dsc -O lv_4.51-7.dsc || exit
wget http://http.debian.net/debian/pool/main/l/lv/lv_4.51.orig.tar.gz -O lv_4.51.orig.tar.gz || exit
wget http://http.debian.net/debian/pool/main/l/lv/lv_4.51-7.debian.tar.xz -O lv_4.51-7.debian.tar.xz || exit

sed -n \
"$(sed -n '/Checksums-Sha256\:/=' $(pwd)/lv_4.51-7.dsc),\
$(sed -n '/Files\:/=' $(pwd)/lv_4.51-7.dsc)p" \
$(pwd)/lv_4.51-7.dsc | grep -v Checksums-Sha256 | grep -v Files |
sed -e "s/601518//" -e "s/52316//" -e "s/ //" >"$LV_4_51_7_SHA256SUMS"
sha256sum -c "$LV_4_51_7_SHA256SUMS" || exit
rm "$LV_4_51_7_SHA256SUMS"

tar zxf $(pwd)/lv_4.51.orig.tar.gz
mv $(pwd)/lv_4.51-7.debian.tar.xz $(pwd)/lv-4.51.orig/
cd $(pwd)/lv-4.51.orig/
tar Jxf $(pwd)/lv_4.51-7.debian.tar.xz
while read line; do
	patch -p1 < $(pwd)/debian/patches/$line
done <$(pwd)/debian/patches/series

rm "/tmp/build/lv_4.51-7" || rm -r "/tmp/build/lv_4.51-7"
test -d "/tmp/build" || mkdir "/tmp/build"
mkdir -p /tmp/build/lv_4.51-7
DESTDIR=/tmp/build/lv_4.51-7
timestamp=$(date +%Y%m%d%H%M%S%N)

$(pwd)/src/configure --prefix=/usr --sysconfdir=/etc --libdir=/usr/doc --mandir=/usr/man --infodir=/usr/info --enable-fastio
make
gzip $(pwd)/lv.1
install -d -m 755 $DESTDIR/usr/bin
install -d -m 755 $DESTDIR/usr/doc/lv
install -d -m 755 $DESTDIR/usr/man/man1
install -m 755 $(pwd)/lv $DESTDIR/usr/bin/lv
install -m 644 $(pwd)/lv.hlp $DESTDIR/usr/doc/lv/lv.hlp
install -m 644 $(pwd)/lv.1.gz $DESTDIR/usr/man/man1/lv.1.gz
install -m 644 $(pwd)/GPL.txt $DESTDIR/usr/doc/lv/GPL.txt
install -m 644 $(pwd)/README $DESTDIR/usr/doc/lv/README
install -m 644 $(pwd)/hello.sample $DESTDIR/usr/doc/lv/hello.sample
install -m 644 $(pwd)/hello.sample.gif $DESTDIR/usr/doc/lv/hello.sample.gif
install -m 644 $(pwd)/index.html $DESTDIR/usr/doc/lv/index.html
install -m 644 $(pwd)/relnote.html $DESTDIR/usr/doc/lv/relnote.html
cd /tmp/build/lv_4.51-7/ || exit
ln -sf lv usr/bin/lgrep
ln -sf lv.1.gz usr/man/man1/lgrep.1.gz
mkdir install
echo "$SLACK_DESC" >install/slack-desc
makepkg -l y -c y /tmp/build/lv-4.51.7-x86_64-$timestamp.txz
upgradepkg /tmp/build/lv-4.51.7-x86_64-$timestamp.txz ||
installpkg /tmp/build/lv-4.51.7-x86_64-$timestamp.txz
cp /tmp/build/lv-4.51.7-x86_64-$timestamp.txz /root/Downloads/
ldconfig

cd

lynx /usr/doc/lv/index.html
