#!/bin/sh
#
# Linux is a Registered Trademark of Linus Torvalds.
#
# Slackware is a registered trademark 
# of Patrick Volkerding and Slackware Linux, Inc.
#
#==============================================================================
#
# Copyright 2019 EarthSimilarityIndex, the 3rd Planet, Solar System
#
# Released under the MIT license
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included
# in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
# OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
# IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
# DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
# TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
# OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
#

if [ ! $USER = root ]; then
	echo "Run this script as root."
	exit
elif [ ! -x /usr/bin/nkf ]; then
	echo "Install nkf first."
	exit
else
	cd /root || exit
	. /etc/profile
	revision=$(date +%Y%m%d%H%M%S%N)
	mv /etc/man.conf /etc/man.conf.bak.$revision
	cat /etc/man.conf.bak.$revision | sed -e "s/\/usr\/bin\/groff -Tnippon -mandocj/\/usr\/bin\/nkf -w \| \/usr\/bin\/groff -Dutf8 -Tutf8 -mandoc -mja -E/" -e "s/\/usr\/bin\/less -is/\/usr\/bin\/less -isR/" -e "s/\/usr\/bin\/less -isRR/\/usr\/bin\/less -isR/" > /etc/man.conf
	echo
	echo 'Changed lines are:'
	grep JNROFF /etc/man.conf | grep -v TROFF
	grep PAGER /etc/man.conf | grep -v HTMLPAGER
	echo
	echo "/etc/man.conf has been backuped to /etc/man.conf.bak.$revision"
	echo
fi