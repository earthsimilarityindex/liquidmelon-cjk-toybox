#!/bin/sh
#
# Linux is a Registered Trademark of Linus Torvalds.
#
# Slackware is a registered trademark
# of Patrick Volkerding and Slackware Linux, Inc.
#
#==============================================================================
#
# Acknowledgments
#
# I've referred the site of uim for the software description.
# Therefore, the license below does not apply to the description.
#
# Copyright (c) 2019 EarthSimilarityIndex, the 3rd Planet, Solar System
#
# Released under the MIT license
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included
# in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
#

SLACK_DESC=$(cat <<EOS
uim: uim 1.8.8 a multilingual input method library and environment.  
uim:  
uim: Its goal is to provide simple, easily extensible and high code-quality 
uim: input method development platform, and useful input method environment 
uim: for users of desktop and embedded platforms.  
uim: 
uim: 
uim: 
uim: 
uim: 
uim: https://github.com/uim/uim  
EOS
)

test -L / && exit 1
test -L /var && exit 1
test -L /var/log && exit 1
test -L /var/log/packages && exit 1
test -L /tmp && exit 1
test -L /tmp/build && exit 1
test -L /root && exit 1
test -L /root/Downloads && exit 1

if test ! "$USER" = root; then
  echo "Run this script as root, bye."
  exit 1
fi

test -d /root/Downloads || mkdir /root/Downloads
cd /root/Downloads || exit 1
rm "uim-1.8.8" || rm -r "uim-1.8.8"
rm "uim-1.8.8.tar.gz" || rm -r "uim-1.8.8.tar.gz"
wget https://github.com/uim/uim/releases/download/1.8.8/uim-1.8.8.tar.gz || exit 1
tar zxf uim-1.8.8.tar.gz
cd uim-1.8.8 || exit 1
./configure \
--prefix=/usr --sysconfdir=/etc --libdir=/usr/lib64 \
--docdir=/usr/doc/uim-1.8.8 --mandir=/usr/man --infodir=/usr/info \
--enable-dict --enable-kde4-applet --enable-notify=stderr,libnotify,knotify4 \
--enable-openssl --with-qt4 --with-qt4-immodule --with-anthy-utf8 --with-curl \
--with-expat --with-ffi
make
rm "/tmp/build/uim-1.8.8" || rm -r "/tmp/build/uim-1.8.8"
mkdir -p /tmp/build/uim-1.8.8 || exit 1
make install DESTDIR=/tmp/build/uim-1.8.8/
cd /tmp/build/uim-1.8.8/ || exit 1
gzip /tmp/build/uim-1.8.8/usr/man/man1/uim-xim.1
mkdir install
echo "$SLACK_DESC" >install/slack-desc
timestamp=$(date +%Y%m%d%H%M%S%N)
makepkg -l y -c y /tmp/build/uim-1.8.8-x86_64-$timestamp.txz
upgradepkg /tmp/build/uim-1.8.8-x86_64-$timestamp.txz ||
installpkg /tmp/build/uim-1.8.8-x86_64-$timestamp.txz
cp /tmp/build/uim-1.8.8-x86_64-$timestamp.txz /root/Downloads/
ldconfig
unset timestamp
unset DESTDIR
unset SLACK_DESC
less /root/Downloads/uim-1.8.8/README
cd || exit 1
exit 0
