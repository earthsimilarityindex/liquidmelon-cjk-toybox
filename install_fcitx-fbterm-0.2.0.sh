#!/bin/sh
#
# Linux is a Registered Trademark of Linus Torvalds.
#
# Slackware is a registered trademark
# of Patrick Volkerding and Slackware Linux, Inc.
#
#==============================================================================
#
# Acknowledgments
#
# I've referred the site of fcitx-fbterm for the software description.
# Therefore, the license below does not apply to the description.
#
# Copyright (c) 2019 EarthSimilarityIndex, the 3rd Planet, Solar System
#
# Released under the MIT license
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included
# in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
#

SLACK_DESC=$(cat <<EOS
fcitx-fbterm: fcitx-fbterm 0.2.0 fcitx-fbterm, add fbterm support to fcitx  
fcitx-fbterm:  
fcitx-fbterm: fcitx-fbterm, add fbterm support to fcitx  
fcitx-fbterm: 
fcitx-fbterm: 
fcitx-fbterm: 
fcitx-fbterm: 
fcitx-fbterm: 
fcitx-fbterm: 
fcitx-fbterm: 
fcitx-fbterm: https://gitlab.com/fcitx/fcitx-fbterm  
EOS
)

test -L / && exit 1
test -L /var && exit 1
test -L /var/log && exit 1
test -L /var/log/packages && exit 1
test -L /tmp && exit 1
test -L /tmp/build && exit 1
test -L /root && exit 1
test -L /root/Downloads && exit 1

if test ! "$USER" = root; then
  echo "Run this script as root, bye."
  exit 1
fi

if test ! -x /usr/bin/fcitx; then
	echo "Install fcitx first."
	exit 1
fi

if test ! -x /usr/bin/fbterm; then
	echo "Install FbTerm first."
	exit 1
fi

test -d /root/Downloads || mkdir /root/Downloads
cd /root/Downloads || exit 1
rm "fcitx-fbterm-0.2.0" || rm -r "fcitx-fbterm-0.2.0"
rm "fcitx-fbterm-0.2.0.tar.gz" || rm -r "fcitx-fbterm-0.2.0.tar.gz"
wget https://gitlab.com/fcitx/fcitx-fbterm/-/archive/0.2.0/fcitx-fbterm-0.2.0.tar.gz || exit
tar zxf fcitx-fbterm-0.2.0.tar.gz
cd fcitx-fbterm-0.2.0 || exit 1
rm "build" || rm -r "build"
mkdir build
cd build || exit 1
cmake .. -DCMAKE_INSTALL_PREFIX=/usr -DLIB_INSTALL_DIR=/usr/lib64
make
rm "/tmp/build/fcitx-fbterm-0.2.0" || rm -r "/tmp/build/fcitx-fbterm-0.2.0"
mkdir -p /tmp/build/fcitx-fbterm-0.2.0 || exit 1
make install DESTDIR=/tmp/build/fcitx-fbterm-0.2.0/
cd /tmp/build/fcitx-fbterm-0.2.0/ || exit 1
mkdir install
echo "$SLACK_DESC" >install/slack-desc
timestamp=$(date +%Y%m%d%H%M%S%N)
makepkg -l y -c y /tmp/build/fcitx-fbterm-0.2.0-x86_64-$timestamp.txz
upgradepkg /tmp/build/fcitx-fbterm-0.2.0-x86_64-$timestamp.txz ||
installpkg /tmp/build/fcitx-fbterm-0.2.0-x86_64-$timestamp.txz
ldconfig
unset timestamp
unset DESTDIR
unset SLACK_DESC
less /root/Downloads/fcitx-fbterm-0.2.0/README
cd || exit 1
exit 0
