#!/bin/sh
#
# Linux is a Registered Trademark of Linus Torvalds.
#
# Slackware is a registered trademark
# of Patrick Volkerding and Slackware Linux, Inc.
#
#==============================================================================
#
# Copyright (c) 2019 EarthSimilarityIndex, the 3rd Planet, Solar System
#
# Released under the MIT license
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included
# in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
#

SLACK_DESC=$(cat <<EOS
mlterm-fb: mlterm-fb 3.8.8 mlterm with framebuffer support  
mlterm-fb:  
mlterm-fb: /usr/bin/mlterm-fb supports framebuffer device. for more 
mlterm-fb: infomation, read /usr/doc/mlterm-3.8.8/en/README.fb  
mlterm-fb: 
mlterm-fb: 
mlterm-fb: 
mlterm-fb: 
mlterm-fb: 
mlterm-fb: 
mlterm-fb: http://mlterm.sourceforge.net/  
EOS
)

#ARCHITECTURE=$(uname -m)
ARCHITECTURE="x86_64"
CFLAGS="-O2 -fPIC"
CXXFLAGS="-O2 -fPIC"
TIMESTAMP=$(date +%Y%m%d%H%M%S%N)
MLTERM_MD5SUM="679be17613aa04857d01264f36c9cb1e  mlterm-3.8.8.tar.gz"
TMP_MLTERM_MD5SUM=$(mktemp)

test -L / && exit 1
test -L /var && exit 1
test -L /var/log && exit 1
test -L /var/log/packages && exit 1
test -L /tmp && exit 1
test -L /tmp/build && exit 1
test -L /root && exit 1
test -L /root/Downloads && exit 1

if test ! "$USER" = root; then
  echo "Run this script as root, bye."
  exit 1
fi

if test ! -x /usr/bin/mlterm; then
  echo "Install mlterm first, bye."
  exit 1
fi

echo "$MLTERM_MD5SUM" >>$TMP_MLTERM_MD5SUM
test -d /root/Downloads || mkdir /root/Downloads
cd /root/Downloads || exit 1
rm "mlterm-3.8.8" || rm -r "mlterm-3.8.8"
rm "mlterm-3.8.8.tar.gz" || rm -r "mlterm-3.8.8.tar.gz"
wget https://sourceforge.net/projects/mlterm/files/01release/mlterm-3.8.8/mlterm-3.8.8.tar.gz \
  -O /root/Downloads/mlterm-3.8.8.tar.gz
md5sum -c "$TMP_MLTERM_MD5SUM" || exit 1
tar zxf mlterm-3.8.8.tar.gz
cd mlterm-3.8.8 || exit 1

./configure \
  --prefix=/usr \
  --libdir=/usr/lib64 \
  --sysconfdir=/etc \
  --localstatedir=/var \
  --mandir=/usr/man \
  --infodir=/usr/info \
  --docdir=/usr/doc/mlterm-fb-3.8.8 \
  --disable-static \
  --with-gui=fb \
  --with-scrollbars \
  --build=$ARCHITECTURE-slackware-linux
make
rm "/tmp/build/mlterm-fb-3.8.8" || rm -r "/tmp/build/mlterm-fb-3.8.8"
mkdir -p /tmp/build/mlterm-fb-3.8.8
make install-fb DESTDIR=/tmp/build/mlterm-fb-3.8.8/
cd /tmp/build/mlterm-fb-3.8.8/ || exit 1
mkdir install
echo "$SLACK_DESC" >install/slack-desc
/sbin/makepkg -l y -c n /tmp/build/mlterm-fb-3.8.8-x86_64-$TIMESTAMP.txz
cd || exit 1
upgradepkg /tmp/build/mlterm-fb-3.8.8-x86_64-$TIMESTAMP.txz ||
installpkg /tmp/build/mlterm-fb-3.8.8-x86_64-$TIMESTAMP.txz

if ! grep -q /usr/lib64/mlterm /etc/ld.so.conf; then
  echo "/usr/lib64/mlterm" >>"/etc/ld.so.conf"
  ldconfig
fi

cp /tmp/build/mlterm-fb-3.8.8-x86_64-$TIMESTAMP.txz /root/Downloads/
rm $TMP_MLTERM_MD5SUM
unset ARCHITECTURE CFLAGS CXXFLAGS DESTDIR TIMESTAMP TMP_MLTERM_MD5SUM MLTERM_MD5SUM SLACK_DESC
less /usr/doc/mlterm-3.8.8/en/README.fb
exit 0
