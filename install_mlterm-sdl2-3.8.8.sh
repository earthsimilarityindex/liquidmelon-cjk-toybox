#!/bin/sh
#
# Linux is a Registered Trademark of Linus Torvalds.
#
# Slackware is a registered trademark
# of Patrick Volkerding and Slackware Linux, Inc.
#
#==============================================================================
#
# Copyright (c) 2019 EarthSimilarityIndex, the 3rd Planet, Solar System
#
# Released under the MIT license
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included
# in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
#

test -L / && exit 1
test -L /var && exit 1
test -L /var/log && exit 1
test -L /var/log/packages && exit 1
test -L /tmp && exit 1
test -L /tmp/build && exit 1
test -L /root && exit 1
test -L /root/Downloads && exit 1

if test ! "$USER" = root; then
  echo "Run this script as root, bye."
  exit 1
fi

if test ! -x /usr/bin/mlterm; then
  echo "Install mlterm first, bye."
  exit 1
fi

#SDL2
if test ! -x /usr/bin/sdl2-config; then
  SOFTWARE_NAME_01="SDL2"
  SOFTWARE_VERSION_01="2.0.9"
  SOFTWARE_SOURCE_URL_01="https://www.libsdl.org/release/SDL2-2.0.9.tar.gz"
  SOFTWARE_ARCHIVE_NAME_01="SDL2-2.0.9.tar.gz"
  SOFTWARE_ARCHIVE_MD5SUM_01="f2ecfba915c54f7200f504d8b48a5dfe"
  SBO_PKG_LOG_NAME_01="SDL2-2.0.9-x86_64-1_SBo"
  SBO_SOURCE_URL_01="https://slackbuilds.org/slackbuilds/14.2/libraries/SDL2.tar.gz"
  SBO_GPG_VERIFY_01="false"
  
  BUILD_ORDER="01"
  
  checksum_md5sum () {
    tempfile=$(mktemp)
    md5sum_data="$2  /root/Downloads/$1/$3"
    echo "$md5sum_data" >"$tempfile"
    unset md5sum_data
    md5sum -c "$tempfile" || exit
    rm "$tempfile" || exit
    unset tempfile
  }

  sbo_builder () {
    if test -f "/var/log/packages/$1" ; then
      echo "$2 $3 is already installed."
      return 1
    else
      test -d "/root/Downloads/$2" && rm -r "/root/Downloads/$2"
      rm "/root/Downloads/$2.tar.gz" || rm -r "/root/Downloads/$2.tar.gz"
      wget "$4" || exit
      if test "$5" = true ; then
        wget "$4.asc" || exit
        gpg --verify "/root/Downloads/$2.tar.gz.asc" "/root/Downloads/$2.tar.gz" || exit
      fi
      tar zxf "/root/Downloads/$2.tar.gz"
      cd "/root/Downloads/$2" || exit
      wget "$6" || exit
      checksum_md5sum "$2" "$7" "$8"
      "/root/Downloads/$2/$2.SlackBuild"
      upgradepkg "/tmp/$1.tgz" || installpkg "/tmp/$1.tgz"
      ldconfig
      cp "/tmp/$1.tgz" /root/Downloads/
      cd /root/Downloads
      return 0
    fi
  }
  cd /root || exit
  . /etc/profile || exit
  test -d /root/Downloads || mkdir /root/Downloads
  cd /root/Downloads
  unset i
  for i in $BUILD_ORDER ; do
    eval sbo_builder \
      "\$SBO_PKG_LOG_NAME_$i" \
      "\$SOFTWARE_NAME_$i" \
      "\$SOFTWARE_VERSION_$i" \
      "\$SBO_SOURCE_URL_$i" \
      "\$SBO_GPG_VERIFY_$i" \
      "\$SOFTWARE_SOURCE_URL_$i" \
      "\$SOFTWARE_ARCHIVE_MD5SUM_$i" \
      "\$SOFTWARE_ARCHIVE_NAME_$i"
  done
fi

if test ! -x /usr/bin/sdl2-config; then
  echo "Install SDL2 first, bye."
  exit 1
fi

SLACK_DESC=$(cat <<EOS
mlterm-sdl2: mlterm-sdl2 3.8.8 mlterm with SDL2 support  
mlterm-sdl2:  
mlterm-sdl2: /usr/bin/mlterm-sdl2 supports SDL2. for more infomation, read 
mlterm-sdl2: /usr/doc/mlterm-3.8.8/en/README.sdl2  
mlterm-sdl2: 
mlterm-sdl2: 
mlterm-sdl2: 
mlterm-sdl2: 
mlterm-sdl2: 
mlterm-sdl2: 
mlterm-sdl2: http://mlterm.sourceforge.net/  
EOS
)
#ARCHITECTURE=$(uname -m)
ARCHITECTURE="x86_64"
CFLAGS="-O2 -fPIC"
CXXFLAGS="-O2 -fPIC"
TIMESTAMP=$(date +%Y%m%d%H%M%S%N)
MLTERM_MD5SUM="679be17613aa04857d01264f36c9cb1e  mlterm-3.8.8.tar.gz"
TMP_MLTERM_MD5SUM=$(mktemp)

echo "$MLTERM_MD5SUM" >>$TMP_MLTERM_MD5SUM
test -d /root/Downloads || mkdir /root/Downloads
cd /root/Downloads || exit 1
rm "mlterm-3.8.8" || rm -r "mlterm-3.8.8"
rm "mlterm-3.8.8.tar.gz" || rm -r "mlterm-3.8.8.tar.gz"
wget https://sourceforge.net/projects/mlterm/files/01release/mlterm-3.8.8/mlterm-3.8.8.tar.gz \
  -O /root/Downloads/mlterm-3.8.8.tar.gz
md5sum -c "$TMP_MLTERM_MD5SUM" || exit 1
tar zxf mlterm-3.8.8.tar.gz
cd mlterm-3.8.8 || exit 1

./configure \
  --prefix=/usr \
  --libdir=/usr/lib64 \
  --sysconfdir=/etc \
  --localstatedir=/var \
  --mandir=/usr/man \
  --infodir=/usr/info \
  --docdir=/usr/doc/mlterm-sdl2-3.8.8 \
  --disable-static \
  --with-gui=sdl2 \
  --with-scrollbars \
  --build=$ARCHITECTURE-slackware-linux
make
rm "/tmp/build/mlterm-sdl2-3.8.8" || rm -r "/tmp/build/mlterm-sdl2-3.8.8"
mkdir -p /tmp/build/mlterm-sdl2-3.8.8
make install-fb DESTDIR=/tmp/build/mlterm-sdl2-3.8.8/
cd /tmp/build/mlterm-sdl2-3.8.8/ || exit 1
mkdir install
echo "$SLACK_DESC" >install/slack-desc
/sbin/makepkg -l y -c n /tmp/build/mlterm-sdl2-3.8.8-x86_64-$TIMESTAMP.txz
cd || exit 1
upgradepkg /tmp/build/mlterm-sdl2-3.8.8-x86_64-$TIMESTAMP.txz ||
installpkg /tmp/build/mlterm-sdl2-3.8.8-x86_64-$TIMESTAMP.txz

if ! grep -q /usr/lib64/mlterm /etc/ld.so.conf; then
  echo "/usr/lib64/mlterm" >>"/etc/ld.so.conf"
  ldconfig
fi

cp /tmp/build/mlterm-sdl2-3.8.8-x86_64-$TIMESTAMP.txz /root/Downloads/
rm $TMP_MLTERM_MD5SUM
unset ARCHITECTURE CFLAGS CXXFLAGS DESTDIR TIMESTAMP TMP_MLTERM_MD5SUM MLTERM_MD5SUM SLACK_DESC
less /usr/doc/SDL2-2.0.9/README.txt
less /usr/doc/mlterm-3.8.8/en/README.sdl2
exit 0
