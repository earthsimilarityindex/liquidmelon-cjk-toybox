#!/bin/sh
#
# Linux is a Registered Trademark of Linus Torvalds.
#
# Slackware is a registered trademark
# of Patrick Volkerding and Slackware Linux, Inc.
#
#==============================================================================
#
# Copyright (c) 2019 EarthSimilarityIndex, the 3rd Planet, Solar System
#
# Released under the MIT license
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included
# in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
#

if test ! -x /usr/bin/fontforge; then
	"Install FontForge first, bye."
	exit
fi

if test $USER = root; then
	echo "Run this script as an ordinal user, bye."
	exit
fi

test -L / && exit
test -L /tmp && exit
test -L /home && exit
cd || exit
test "$(pwd)" = "$HOME" || exit
test -L "$HOME" && exit
test -L "$HOME"/Downloads && exit
test -L "$HOME"/.fonts && exit

test -d "$HOME"/Downloads || mkdir "$HOME"/Downloads
cd "$HOME"/Downloads

rm "$HOME"/Downloads/Ricty-4.1.1 || rm -r "$HOME"/Downloads/Ricty-4.1.1
mkdir "$HOME"/Downloads/Ricty-4.1.1

cd "$HOME"/Downloads/Ricty-4.1.1 || exit

wget https://www.rs.tus.ac.jp/yyusa/ricty/ricty_generator.sh || exit
wget https://www.rs.tus.ac.jp/yyusa/ricty/os2version_reviser.sh || exit
wget https://github.com/google/fonts/raw/master/ofl/inconsolata/Inconsolata-Bold.ttf || exit
wget https://github.com/google/fonts/raw/master/ofl/inconsolata/Inconsolata-Regular.ttf || exit
wget https://ja.osdn.net/dl/mix-mplus-ipa/migu-1m-20150712.zip || exit
unzip -j migu-1m-20150712.zip migu-1m-20150712/migu-1m-bold.ttf migu-1m-20150712/migu-1m-regular.ttf

chmod +x $(pwd)/ricty_generator.sh
$(pwd)/ricty_generator.sh auto

rm "$HOME"/.fonts/Ricty-4.1.1 || rm -r "$HOME"/.fonts/Ricty-4.1.1
mkdir "$HOME"/.fonts/Ricty-4.1.1
cp Ricty*.ttf "$HOME"/.fonts/Ricty-4.1.1
fc-cache -fv

cd
