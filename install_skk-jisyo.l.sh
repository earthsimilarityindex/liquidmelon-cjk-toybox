#!/bin/sh
#
# Linux is a Registered Trademark of Linus Torvalds.
#
# Slackware is a registered trademark
# of Patrick Volkerding and Slackware Linux, Inc.
#
#==============================================================================
#
# Copyright (c) 2019 EarthSimilarityIndex, the 3rd Planet, Solar System
#
# Released under the MIT license
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included
# in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
#

version=$(date +%Y%m)

SLACK_DESC=$(cat <<EOS
skk-jisyo.l: skk-jisyo.l $version SKK JISYO L  
skk-jisyo.l:  
skk-jisyo.l: This is a large dictionary file for SKK (Simple Kana to Kanji 
skk-jisyo.l: conversion program).  
skk-jisyo.l: 
skk-jisyo.l: 
skk-jisyo.l: 
skk-jisyo.l: 
skk-jisyo.l: http://openlab.ring.gr.jp/skk/dic.html  
skk-jisyo.l: 
skk-jisyo.l: https://skk-dev.github.io/dict/  
EOS
)

test -L / && exit
test -L /var && exit
test -L /var/log && exit
test -L /var/log/packages && exit
test -L /tmp && exit
test -L /tmp/build && exit
test -L /root && exit
test -L /root/Downloads && exit

if test ! "$USER" = root; then
  echo "Run this script as root, bye."
  exit
fi

test -d /root/Downloads || mkdir /root/Downloads
cd /root/Downloads || exit
rm "SKK-JISYO.L" || rm -r "SKK-JISYO.L"
mkdir "$(pwd)/SKK-JISYO.L"
cd "$(pwd)/SKK-JISYO.L" || exit
wget https://skk-dev.github.io/dict/SKK-JISYO.L.gz || exit
wget https://skk-dev.github.io/dict/SKK-JISYO.L.gz.md5 || exit
md5sum -c "$(pwd)/SKK-JISYO.L.gz.md5" || exit

test -d /tmp/build || mkdir /tmp/build
rm "/tmp/build/SKK-JISYO.L" || rm -r "/tmp/build/SKK-JISYO.L"
mkdir -p /tmp/build/SKK-JISYO.L/usr/share/skk/
zcat "$(pwd)/SKK-JISYO.L.gz" >/tmp/build/SKK-JISYO.L/usr/share/skk/SKK-JISYO.L

timestamp=$(date +%Y%m%d%H%M%S%N)
cd /tmp/build/SKK-JISYO.L/
mkdir install
echo "$SLACK_DESC" >install/slack-desc
makepkg -l y -c y /tmp/build/skk-jisyo.l-$version-noarch-$timestamp.txz
upgradepkg /tmp/build/skk-jisyo.l-$version-noarch-$timestamp.txz ||
installpkg /tmp/build/skk-jisyo.l-$version-noarch-$timestamp.txz

unset timestamp
cd

# lynx http://openlab.ring.gr.jp/skk/dic.html
