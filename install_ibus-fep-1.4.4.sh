#!/bin/sh
#
# Linux is a Registered Trademark of Linus Torvalds.
#
# Slackware is a registered trademark
# of Patrick Volkerding and Slackware Linux, Inc.
#
#==============================================================================
#
# Acknowledgments
#
# I've referred the sites of libfep and ibus-fep for the software description.
# Therefore, the license below does not apply to the descriptions.
#
# Copyright (c) 2019 EarthSimilarityIndex, the 3rd Planet, Solar System
#
# Released under the MIT license
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included
# in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
#

SLACK_DESC_LIBFEP=$(cat <<EOS
libfep: libfep 0.1.0 a library to implement FEP on ANSI terminals  
libfep:  
libfep: a library to implement FEP (front end processor) on ANSI terminals  
libfep: 
libfep: Features:  
libfep: * Render text at the bottom of the terminal.  
libfep: * Render text at the cursor position.  
libfep: * Send text to the child process.  
libfep: * Monitor key strokes typed on the terminal.  
libfep: 
libfep: https://github.com/ueno/libfep  
EOS
)

SLACK_DESC_IBUS_FEP=$(cat <<EOS
ibus-fep: ibus-fep 1.4.4 ibus-fep -- IBus client for text terminals  
ibus-fep:  
ibus-fep: ibus-fep is merely an IBus client like the GTK IM module 
ibus-fep: (im-ibus.so), not a bus nor panel replacement. So, before running 
ibus-fep: ibus-fep, make sure that ibus-daemon is running.  
ibus-fep: 
ibus-fep: 
ibus-fep: 
ibus-fep: 
ibus-fep: 
ibus-fep: https://github.com/ueno/ibus-fep  
EOS
)

test -L / && exit 1
test -L /var && exit 1
test -L /var/log && exit 1
test -L /var/log/packages && exit 1
test -L /tmp && exit 1
test -L /tmp/build && exit 1
test -L /root && exit 1
test -L /root/Downloads && exit 1

if test ! "$USER" = root; then
  echo "Run this script as root, bye."
  exit 1
fi

if test ! -x /usr/bin/ibus; then
  echo "Install ibus first, bye."
  exit 1
fi

test -d /root/Downloads || mkdir /root/Downloads
cd /root/Downloads || exit 1

rm "libfep-0.1.0" || rm -r "libfep-0.1.0"
mkdir "$(pwd)/libfep-0.1.0"
cd "$(pwd)/libfep-0.1.0" || exit 1
wget https://github.com/ueno/libfep/releases/download/0.1.0/libfep-0.1.0.tar.gz || exit 1
tar zxf libfep-0.1.0.tar.gz
cd "$(pwd)/libfep-0.1.0" || exit 1
$(pwd)/configure --prefix=/usr --sysconfdir=/etc --libdir=/usr/lib64 \
--mandir=/usr/man --infodir=/usr/info --docdir=/usr/doc/libfep-0.1.0
make
test -d /tmp/build || mkdir /tmp/build
rm "/tmp/build/libfep-0.1.0" || rm -r "/tmp/build/libfep-0.1.0"
mkdir /tmp/build/libfep-0.1.0
make install DESTDIR=/tmp/build/libfep-0.1.0/
cd /tmp/build/libfep-0.1.0/ || exit 1
mkdir install
echo "$SLACK_DESC_LIBFEP" >install/slack-desc
timestamp=$(date +%Y%m%d%H%M%S%N)
makepkg -l y -c y /tmp/build/libfep-0.1.0-x86_64-$timestamp.txz
upgradepkg /tmp/build/libfep-0.1.0-x86_64-$timestamp.txz ||
installpkg /tmp/build/libfep-0.1.0-x86_64-$timestamp.txz
ldconfig
cp /tmp/build/libfep-0.1.0-x86_64-$timestamp.txz /root/Downloads
unset timestamp
unset SLACK_DESC_LIBFEP
cd /root/Downloads || exit 1

rm "ibus-fep-1.4.4" || rm -r "ibus-fep-1.4.4"
mkdir "$(pwd)/ibus-fep-1.4.4"
cd "$(pwd)/ibus-fep-1.4.4" || exit 1
wget https://github.com/ueno/ibus-fep/archive/1.4.4.tar.gz || exit 1
tar zxf 1.4.4.tar.gz
cd "$(pwd)/ibus-fep-1.4.4" || exit 1
$(pwd)/autogen.sh --prefix=/usr --sysconfdir=/etc --libdir=/usr/lib64 \
--mandir=/usr/man --infodir=/usr/info --docdir=/usr/doc/ibus-fep-1.4.4
make
test -d /tmp/build || mkdir /tmp/build
rm "/tmp/build/ibus-fep-1.4.4" || rm -r "/tmp/build/ibus-fep-1.4.4"
mkdir /tmp/build/ibus-fep-1.4.4
make install DESTDIR=/tmp/build/ibus-fep-1.4.4/
cd /tmp/build/ibus-fep-1.4.4/ || exit 1
mkdir install
echo "$SLACK_DESC_IBUS_FEP" >install/slack-desc
timestamp=$(date +%Y%m%d%H%M%S%N)
makepkg -l y -c y /tmp/build/ibus-fep-1.4.4-x86_64-$timestamp.txz
upgradepkg /tmp/build/ibus-fep-1.4.4-x86_64-$timestamp.txz ||
installpkg /tmp/build/ibus-fep-1.4.4-x86_64-$timestamp.txz
ldconfig
cp /tmp/build/ibus-fep-1.4.4-x86_64-$timestamp.txz /root/Downloads
unset timestamp
unset SLACK_DESC_IBUS_FEP
cd || exit 1

less /root/Downloads/libfep-0.1.0/libfep-0.1.0/README
less /root/Downloads/ibus-fep-1.4.4/ibus-fep-1.4.4/README

exit 0
